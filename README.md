#Haar-Camshift face tracker#

Implement with OpenCV-Python

-----

Step by step

1. Install Python 2.7
2. Install Numpy using pip (pip install numpy)
3. Download OpenCV
4. Goto opencv/build/python/2.7 folder
5. Copy cv.pyd to Lib/site-packages/ under Python installed directory (default is "C:/Python27")
6. Run haar-camshift.py, see the results
