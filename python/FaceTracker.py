import numpy as np
import cv2

class FaceTracker:
    def __init__(self, faceCasadeFilename, eyeCasadeFileName):
        self.__termCrit = ( cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1 )

        self.__faceCascade = cv2.CascadeClassifier(faceCasadeFilename)
        self.__eyeCascade = cv2.CascadeClassifier(eyeCasadeFileName)
        self.__roiHist = None
        self.__face = None
        self.__eyes = None
        
        self.traced = None
        self.state = 0

    def traceFace(self, frame):
        if self.state != 1 and self.state != 2:
            self.__detectFace(frame)
        else:
            self.__trackFace(frame)

        return self.state

    def __detectFace(self, frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = self.__faceCascade.detectMultiScale(gray, 1.3, 5)

        if len(faces) == 0:
            return 0

        self.__face = faces[0]

        (x,y,w,h) = self.__face

        self.traced = (x,y,w,h)

        roi_gray = gray[y:y+h, x:x+w]
        self.__eyes = self.__eyeCascade.detectMultiScale(roi_gray)

        self.state = 1

        return self.state

    def __trackFace(self, frame):
        if self.state < 1:
            self.state = 0
            return 0

        if self.state == 1:
            #setting up
            (x,y,w,h) = self.__face
            self.traced = (x,y,w,h)
            if len(self.traced) == 0:
                self.state = 0
                return self.state

            (x,y,w,h) = self.traced

            roi = frame[y:y+h,x:x+w]

            hsv_roi = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)
            #mask = cv2.inRange(hsv_roi, np.array((0., 60.,32.)), np.array((180.,255.,255.)))
            mask = cv2.inRange(hsv_roi, np.array((0., 30., 10.)), np.array((180.,255.,255.)))
            self.__roiHist = cv2.calcHist([hsv_roi], [0], mask, [180], [0,180])
            cv2.normalize(self.__roiHist, self.__roiHist, 0, 255, cv2.NORM_MINMAX)

            self.state = 2

        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        dst = cv2.calcBackProject([hsv], [0], self.__roiHist, [0,180], 1)

        ret, self.traced = cv2.meanShift(dst, self.traced, self.__termCrit)

        return self.state
