import numpy as np
import cv2
import FaceTracker

faceTracker = FaceTracker.FaceTracker('haarcascade_frontalface_default.xml', 'haarcascade_eye.xml')

cap = cv2.VideoCapture(0) # init cam
state = 0

while(True):
    
    ret, frame = cap.read()

    state = faceTracker.traceFace(frame)

    if faceTracker.traced != None:
        (x,y,w,h) = faceTracker.traced
        cv2.rectangle(frame, (x,y), (x+w,y+h), (0, 255, 0), 2)

    cv2.imshow('frame', frame)

    key = cv2.waitKey(30)

    if (key & 0xFF) == ord('q'):
        break
    elif (key & 0xFF) == ord('r'):
        faceTracker.state = 0 # reset

cap.release()
cv2.destroyAllWindows()


